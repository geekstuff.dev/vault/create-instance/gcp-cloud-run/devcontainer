#!/bin/sh

set -e

# Few utilities
if command -v apk 1>/dev/null 2>/dev/null; then
    echo "TO IMPLEMENT"
    exit 1
elif command -v apt 1>/dev/null 2>/dev/null; then
    apt-get update
    apt-get -y install dnsutils
    rm -rf /var/lib/apt/lists/*
fi

# Append
cat <<- "EOF" >> /usr/local/bin/devcontainer-components
vault -version | cut -sd' ' -f-2
EOF
